#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias pac='sudo pacman'
alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '
PS1='$ '


neofetch
date
set -o vi
