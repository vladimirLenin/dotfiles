--------------------------------------------------------------------------------
-- | Example.hs
--
-- Example configuration file for xmonad using the latest recommended
-- features (e.g., 'desktopConfig').
module Main (main) where

--------------------------------------------------------------------------------
import Custom.Keys
import Custom.Layouts
import Custom.Variables
import Custom.MyAutoStart
--import Custom.MyWorkSpaces
import Custom.MyProject
import XMonad.Util.EZConfig

    -- Base
import XMonad
import System.IO (hPutStrLn)


    -- Hooks
import XMonad.Hooks.WorkspaceHistory
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.ManageDocks (docksEventHook, manageDocks)
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ServerMode

    -- Layout
import XMonad.Layout.ShowWName
import XMonad.Util.NamedActions
    -- Actions
--import XMonad.Actions.GridSelect
import XMonad.Actions.Navigation2D
import XMonad.Actions.DynamicProjects
--import XMonad.Actions.WorkspaceCursors
--import qualified XMonad.Actions.TreeSelect as TS (toWorkspaces )
    -- Util
import XMonad.Util.EZConfig  (additionalKeysP)
import XMonad.Util.Run (spawnPipe)
--import XMonad.Actions.Plane
--import Data.Map (union)
--------------------------------------------------------------------------------
main :: IO()
main = do

  xmproc0 <- (spawnPipe "xmobar  /home/vamshi/.xmobarrc" )

  -- Start xmonad using the main desktop configuration with a few
  -- simple overrides:
  xmonad $ dynamicProjects projects
            $  withNavigation2DConfig def
              $ additionalNav2DKeys (xK_Up, xK_Left, xK_Down, xK_Right)
                                    [(mod4Mask,               windowGo  ),
                                     (mod4Mask .|. shiftMask, windowSwap)]
                                    False
              $ ewmh def
    { modMask     = myModMask -- Use the "Win" key for the mod key
    , layoutHook         = showWName' myShowWNameTheme myLayouts
    , manageHook = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageDocks
    -- Run xmonad commands from command line with "xmonadctl command". Commands include:
    -- shrink, expand, next-layout, default-layout, restart-wm, xterm, kill, refresh, run,
    -- focus-up, focus-down, swap-up, swap-down, swap-master, sink, quit-wm. You can run
    -- "xmonadctl 0" to generate full list of commands written to ~/.xsession-errors.
    , handleEventHook    = serverModeEventHookCmd
    
                       <+> serverModeEventHook
                       <+> serverModeEventHookF "XMONAD_PRINT" (io . putStrLn)
                       <+> docksEventHook
    , startupHook = myStartupHook
    , terminal    = myTerminal
    , borderWidth =  myBorderWidth         -- Sets border width for windows
    , normalBorderColor   =  myNormalBorderColor  -- Border color of normal windows
    , focusedBorderColor  =  myFocusedBorderColor  -- Border color of focused windows
    , workspaces         = myWorkspaces
    , logHook = workspaceHistoryHook <+> myLogHook <+> dynamicLogWithPP xmobarPP
                        { ppOutput = \x -> hPutStrLn xmproc0 x -- >> hPutStrLn xmproc1 x  >> hPutStrLn xmproc2 x
                        , ppCurrent = xmobarColor "#c3e88d" "" . wrap "[" "]" -- Current workspace in xmobar
                        , ppVisible = xmobarColor "#c3e88d" ""                -- Visible but not current workspace
                        --, ppHidden = xmobarColor "#82AAFF" ""
                        , ppHidden = xmobarColor "#82AAFF" "" . wrap "*" ""   -- Hidden workspaces in xmobar
                        -- , ppHiddenNoWindows = xmobarColor "#F07178" ""        -- Hidden workspaces (no windows)
                        , ppHiddenNoWindows= \( _ ) -> ""       -- Only shows visible workspaces. Useful for TreeSelect.
                        , ppTitle = xmobarColor "#d0d0d0" "" . shorten 60     -- Title of active window in xmobar
                        , ppSep =  "<fc=#666666> | </fc>"                     -- Separators in xmobar
                        , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
                        , ppExtras  = [windowCount]                           -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                        }
       } `additionalKeysP` myKeys
     $ addDescrKeys ((mod4Mask, xK_F1), xMessage) myKeys'
                   def { modMask = mod4Mask }
  {-
myKeys' conf = union (keys def conf) $ myNewKeys conf

myNewKeys (XConfig {modMask = modm}) = planeKeys modm (Lines 3) Finite--}
-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Sans:bold:size=60"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#0F0F0F"
    , swn_color             = "#FFFFFF"
    }


myWorkspaces :: [String]
myWorkspaces = ["dop"]


myKeys' c = (subtitle "Custom Keys":) $ mkNamedKeymap c $
   [("M-x a", addName "useless message" $ spawn "xmessage foo"),
    ("M-c", sendMessage' Expand)]
    ^++^
   [("<XF86AudioPlay>", spawn "mpc toggle" :: X ()),
    ("<XF86AudioNext>", spawn "mpc next")]
-- Sets opacity for inactive (unfocused) windows. I prefer to not use
-- this feature so I've set opacity to 1.0. If you want opacity, set
-- this to a value of less than 1 (such as 0.9 for 90% opacity).
myLogHook :: X ()
myLogHook = fadeInactiveLogHook fadeAmount
    where fadeAmount = 0.89

{-
myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                  (0x29,0x2d,0x3e) -- lowest inactive bg
                  (0x29,0x2d,0x3e) -- highest inactive bg
                  (0xc7,0x92,0xea) -- active bg
                  (0xc0,0xa7,0x9a) -- inactive fg
                  (0x29,0x2d,0x3e) -- active fg

-- gridSelect menu layout
mygridConfig :: p -> GSConfig Window
mygridConfig _ = (buildDefaultGSConfig myColorizer)
    { gs_cellheight   = 70
    , gs_cellwidth    = 340
    , gs_cellpadding  = 6
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
    , gs_font         = myFont
-}

--------------------------------------------------------------------------------
-- | Manipulate windows as they are created.  The list given to
-- @composeOne@ is processed from top to bottom.  The first matching
-- rule wins.
--
-- Use the `xprop' tool to get the info you need for these matches.
-- For className, use the second value that xprop gives you.
myManageHook :: ManageHook
myManageHook = composeOne
  [ className =? "Pidgin" -?> doFloat
  , className =? "XCalc"  -?> doFloat
  , className =? "mpv"    -?> doFloat
  , isDialog              -?> doCenterFloat

    -- Move transient windows to their parent:
  , transience
  ]
