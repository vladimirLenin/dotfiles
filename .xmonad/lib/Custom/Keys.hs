

module Custom.Keys where

    -- Custom
import Custom.MyPrompt
import Custom.Variables
--import Custom.MyTreeSelect
--import Custom.MyWorkSpaces

    -- Base
import XMonad
--import System.Exit
--import qualified XMonad.StackSet as W

    -- Hooks
import XMonad.Hooks.ManageDocks (ToggleStruts(..))

    -- Layout
import XMonad.Layout.ToggleLayouts (ToggleLayout(..))

    -- Prompt
--import XMonad.Prompt
--import XMonad.Prompt.Shell
--import XMonad.Prompt.Window
import XMonad.Prompt.XMonad
--import XMonad.Prompt.Workspace
--import XMonad.Prompt.ConfirmPrompt
--import XMonad.Prompt.AppLauncher as AL

    -- Actions
import XMonad.Actions.WithAll
import XMonad.Actions.SimpleDate
import XMonad.Actions.TagWindows
import qualified XMonad.Actions.Search as S
--import qualified XMonad.Actions.TreeSelect as TS ( treeselectWorkspace )
import XMonad.Actions.CycleWS
--import XMonad.Actions.GridSelect
--import XMonad.Actions.CopyWindow(copy)
import XMonad.Actions.DynamicWorkspaces


myKeys :: [(String, X())]
myKeys =     -- Add some extra key bindings:
      [
        -- Exit XMonad
       -- ("M-<Esc>",   confirmPrompt myXPConfig "exit" (io exitSuccess))
      --, ("M1-q", spawn "./repos/xmenu/xmenu.sh")          -- XMenu (Custom)
       ("M-f", sendMessage (Toggle "Full"))          -- Full screen (Not removes xmobar)
      , ("M-q" , kill)
      , ("M1-q" , removeEmptyWorkspace )
      , ("M-S-q", killAll)                                  -- Kill Current Window
      , ("M1-d" , date)                                    -- Uses Dzen2 to show Date
      , ("M-u" , withFocused (addTag "abc"))
      , ("M-w", xmonadPromptC mainPrompt mainPromptXPConfig )
      , ("M-<Esc>", spawn "clearine" )
      --, ("M1-y", searchPrompt)
      , ("M-e", spawn "emacsclient -c -a '' --eval '(eshell)'")                            -- Run Emacs
      , ("M-S-r", spawn "xmonad --recompile ; xmonad --restart")        -- Restarts xmonad
      , ("M-<Return>", spawn myTerminal )                 -- Terminal
      , ("M-S-<Space>", sendMessage ToggleStruts)         -- Toggles struts
      , ("M-<Tab>", nextWS)
      , ("M1-<Tab>", prevWS)
      , ("M-2", spawn "scrot")
      --, ("M1-<Tab>" ,  goToSelected $ mygridConfig myColorizer)  -- goto selected window
      --, ("C-g b" , bringSelected $ mygridConfig myColorizer) -- bring selected window
      --,( "M1-a" , AL.launchApp appXPConfig "xdg-open" )
--      , ("C-t t", TS.treeselectWorkspace tsDefaultConfig myWorkspaces W.greedyView)
      -- tree select choose workspace to send window
--      , ("C-t g", TS.treeselectWorkspace tsDefaultConfig myWorkspaces W.shift)
      ]
      -- Appending search engine prompts to keybindings list.
      -- Look at "search engines" section of this config for values for "k".
      ++ [("M-s " ++ k, S.promptSearch myXPConfig f) | (k,f) <- searchList ]
      ++ [("M-S-s " ++ k, S.selectSearch f) | (k,f) <- searchList ]

