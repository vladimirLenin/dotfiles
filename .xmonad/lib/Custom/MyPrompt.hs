-- | 

module Custom.MyPrompt where

    -- Custom
import Custom.Variables
--import Custom.GridSelect

    -- Base
import XMonad
import System.Exit
    -- Action
import qualified XMonad.Actions.Search as S
import XMonad.Actions.DynamicProjects
--import XMonad.Actions.DynamicWorkspaces
--import XMonad.Actions.CopyWindow(copy)
    -- Prompt
import XMonad.Prompt
import XMonad.Prompt.Input
import XMonad.Prompt.XMonad
import XMonad.Prompt.FuzzyMatch
--import XMonad.Prompt.AppLauncher as AL
import XMonad.Prompt.Window
--import XMonad.Prompt.Theme--import XMonad.Prompt.Shell
import XMonad.Prompt.RunOrRaise

------------------------------------------------------------------------
-- SEARCH ENGINES
------------------------------------------------------------------------
-- Xmonad has several search engines available to use located in
-- XMonad.Actions.Search. Additionally, you can add other search engines
-- such as those listed below.
archwiki, ebay, news, reddit, urban :: S.SearchEngine

archwiki = S.searchEngine "archwiki" "https://wiki.archlinux.org/index.php?search="
ebay     = S.searchEngine "ebay" "https://www.ebay.com/sch/i.html?_nkw="
news     = S.searchEngine "news" "https://news.google.com/search?q="
reddit   = S.searchEngine "reddit" "https://www.reddit.com/search/?q="
urban    = S.searchEngine "urban" "https://www.urbandictionary.com/define.php?term="

-- This is the list of search engines that I want to use. Some are from
-- XMonad.Actions.Search, and some are the ones that I added above.
searchList :: [(String, S.SearchEngine)]
searchList = [ ("a", archwiki)
             , ("d", S.duckduckgo)
             , ("e", ebay)
             , ("g", S.google)
             , ("h", S.hoogle)
             , ("i", S.images)
             , ("n", news)
             , ("r", reddit)
             , ("s", S.stackage)
             , ("t", S.thesaurus)
             , ("v", S.vocabulary)
             , ("b", S.wayback)
             , ("u", urban)
             , ("w", S.wikipedia)
             , ("y", S.youtube)
             , ("z", S.amazon)
             ]


--------------------------------------------------------------------------------
-- | Customize the way 'XMonad.Prompt' looks and behaves.  It's a
-- great replacement for dzen.
myXPConfig :: XPConfig
myXPConfig = def
  { position          = Top
  , bgColor           = "#000000"
  , fgColor           = "#DDDDDD"
  , fgHLight          = "#FFFFFF"
  , bgHLight          = "#333333"
  , borderColor       = "#FFFFFF"
  , alwaysHighlight   = True
  , promptBorderWidth = 1
  , font              = "xft:LucidaGrande:size=14"
  , height            = 36
  , searchPredicate   = fuzzyMatch
  }

appXPConfig :: XPConfig
appXPConfig = def
                {
                                     position            = CenteredAt { xpCenterY = 0.2, xpWidth = 0.5 }
                                     , bgColor           = "grey7"
                                     , fgColor           = "grey80"
                                     , bgHLight          = "#02bfa0"
                                     , fgHLight          = "White"
                                     , borderColor       = "white"
                                     , alwaysHighlight   = True
                                     , promptBorderWidth = 4
                                     , defaultText       = []
                                     , font              = "xft:LucidaGrande:size=34"
                                     , height            = 96
                                     , searchPredicate   = fuzzyMatch
                                     }

myXPConfig' :: XPConfig
myXPConfig' = appXPConfig
             {
               autoComplete      = Just 100000    -- set Just 100000 for .1 sec
               --, position          = CenteredAt { xpCenterY = 2.85, xpWidth = 0.7 }
             }

searchPrompt :: X()
searchPrompt = inputPrompt myXPConfig "yay " ?+ appStore

appStore :: String -> X()
appStore query =  spawn (((myTerminal ++) " -e yay " ++ ) query)



mainPromptXPConfig :: XPConfig
mainPromptXPConfig = myXPConfig' {
                            font              = "xft:LucidaMAC:size=34"
                            , bgHLight        =  "#3851ab"
                          , promptBorderWidth = 6
                          , position            = CenteredAt { xpCenterY = 0.3, xpWidth = 0.57 }
                          , height            = 120
                          , alwaysHighlight     = True
                          , borderColor         = "#939392"
                         }
goToXPConfig :: XPConfig
goToXPConfig = mainPromptXPConfig {
                                   bgHLight           = "#2a7d30"
                                  , position          = Top
                                  , height            = 120
                                  , promptBorderWidth = 4
                                  , font              = "xft:LucidaGrande:size=27"
                                  }



-- A list of all of the standard Xmonad prompts and a key press assigned to them.
-- These are used in conjunction with keybinding I set later in the config.
mainPrompt :: [(String , X ())]
mainPrompt = [ {-("m", manPrompt)          -- manpages prompt
             , ("p", passPrompt)         -- get passwords (requires 'pass')
             , ("g", passGeneratePrompt) -- generate passwords (requires 'pass')
             , ("r", passRemovePrompt)   -- remove passwords (requires 'pass')
             , ("s", sshPrompt)  --}        -- ssh prompt

              ("switchProject" , switchProjectPrompt  goToXPConfig)

             --, ("appLauncher" , AL.launchApp appXPConfig "Launch:" )
             , ("runOrRaise" , runOrRaisePrompt appXPConfig)
             --, ("search", shellPrompt myXPConfig )       -- xmonad prompt
             , ("window", windowPrompt myXPConfig'  Goto allWindows)
             , ("throwWindow" , shiftToProjectPrompt myXPConfig)
             , ("playStore", searchPrompt)
             , ("exit",xmonadPromptC exitPrompt exitPromptXPConfig)

            -- , ("exit" ,spawnSelected' myList  )
             --, ("themePrompt",themePrompt myXPConfig)
             ]

exitPromptXPConfig :: XPConfig
exitPromptXPConfig = myXPConfig' {
                       position = CenteredAt {xpCenterY = 0.3 , xpWidth = 0.84}
                       , height            = 300
                       , bgHLight           = "#0f0f0f"
                       , promptBorderWidth = 3
                       , font              = "xft:LucidaGrande:size=32"
                     }
exitPrompt :: [(String, X ())]
exitPrompt = [
        ("shutdown", spawn "sudo poweroff"),
        ("reboot", spawn "sudo reboot"),
        ("exit", io $ exitWith ExitSuccess),
        --("Lock", spawn "xtrlock -b"),
        ("xRestart", restart "xmonad" True)
    ]
