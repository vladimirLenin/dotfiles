-- | 

module Custom.MyProject where


    -- Base
import XMonad

    -- Action
import XMonad.Actions.DynamicProjects


projects :: [Project]
projects =[ Project { projectName      = "browser"
            , projectDirectory = "~/Download"
            , projectStartHook = Just $ do  spawn "brave"
            }

  , Project { projectName      = "editor"
            , projectDirectory = "~/"
            , projectStartHook = Just $ do  spawn "emacs"
            }
 , Project { projectName      = "spotify"
            , projectDirectory = "~/"
            , projectStartHook = Just $ do  spawn "flatpak run com.spotify.Client"
           }
    , Project { projectName      = "zbluetooth"
            , projectDirectory = "~/"
            , projectStartHook = Just $ do  spawn "konsole -e bluetoothctl"
                                            spawn "pavucontrol"
            }
  ]
